1
```
String.prototype.repeat = function (num) {
	var rString = '',
	i;
	
	for (i = 0; i < num; i++) {
		rString += this;
	}
	
	return rString;
} 

console.log('Codepen must die! '.repeat(5))
```

2
```
var myObjectKeys = function (obj) {
	if (!(typeof obj === 'object' && !!obj)) return []; 
	
	var keys = []; 
	
    for (var key in obj) {
    	if (obj.hasOwnProperty(key)) {
    		keys.push(key);
    	}
    }
    return keys;  
}
```

3 [Sources](https://repl.it/DrUi)

4 [Sources](https://repl.it/DrUd)

4.1 [Sources](https://repl.it/DrTT/9)