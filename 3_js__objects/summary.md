# Objects
## Створення

Два способи створити об'єкт:
- літеральний
```
var obj = {
    myKey: 'value'
};
```
- через конструктор
```
var obj = new Object();

obj.myKey = 'value';
```

При додаванні властивості в об'єкт викликається внутрішній метод [[Put]], який виділяє 
пам'ять під данні та присвоює властивості деякі параметри за замовчуванням.
Такі властивості є _власними властивостями_ об'єкта (тобто, знаходяться в цьому екземплярі, 
а не в лянцюгу прототипів).

При зміні значення властивості викликається внутрішній метод [[Set]], який змінює попереднє
значення на нове.

## Доступ до властивостей

Властивості можуть додаватись в об'єкт в будь-який момент виконання коду. Через це, перед тим, як зчитувати
властивість об'єкту, варто переконатись, що вона існує.

Це можна зробити декількома способами:
- надія на falsy-значення

falsy вважаються значення тип яких, при порівнянні, приводяться до false - NaN, 0, undefined, "", null.

```
obj = {
    foo: 'val'
}

// якщо foo є в об'єкті, то з ним можна працювати
if (obj.foo) {
    globalFoo = obj.foo;
}
```

І все це гарно працює, якщо foo - `undefined` (неоголошена змінна/властивість) або `null`. 
Проблеми будуть у випадку, коли foo буде містити в собі порожню строку, 0 і т.д.

- `in`

В більшості випадків in є найбільш правильним способом перевірити наявність властивості в об'єкті.

```
obj = {
    foo: function () {}
}

// якщо foo є в об'єкті, то з ним можна працювати
if ('foo' in obj) {
    obj.foo();
}
```

Недоліком може бути тільки те, що `in` також перевіряє наявність властивості в батьківських об'єктах (прототипах).

- 'hasOwnProperty'

Для перевірки наявності властивості _ТІЛЬКИ_ в даному екземплярі об'єкта використовується саме метод hasOwnProperty.

```
obj = {
    foo: function () {}
}

if (obj.hasOwnProperty('foo')) {
    obj.foo(); // виконається
}

if (obj.hasOwnProperty('toString')) {
    obj.toString(); // не виконається
}
```

## Видалення об'єктів

Видаляти об'єкти можна з допомогою оператора delete.

При цьому викликається внутрішній метод [[Delete]].

```
obj = {
    foo: function () {}
}

delete obj.foo;

console.log(obj.hasOwnProperty('foo')); //false
```

## Перебір властивостей

Для перебору властивостей об'єкту використовуються цикли. В переборі беруть участь тільки ті властивості, у яких
внутрішній атрибут [[Enumerable]] рівний true.

Є декілька варіантів використання циклів:

- for .. in

```
    for (var prop in obj) {
        console.log("obj[" + prop + "] = " + obj[prop]);
    }
```

`for .. in` витягує властивості не тільки з самого об'єкту, а і з його ланцюга прототипів.

Виправити це можна перевіркою з hasOwnProperty:

```
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            console.log("obj[" + prop + "] = " + obj[prop]);
        }
    }
```

Або використовувати другий варіант.

- Object.keys()

Object.keys() - повертає тільки назви _ВЛАСНИХ_ об'єктів.

```
var objKeys = Object.keys(obj),
    len,
    i;

for (i = 0, len = objKeys.length; i < len; i++) {
     console.log("obj[" + objKeys[i] + "] = " + obj[prop[i]]);
}
```

Перевірити, чи буде властивість брати участь у переборі можна методом propertyIsEnumerable: 

```
obj.propertyIsEnumerable('foo');    //true
obj.propertyIsEnumerable('length'); //false
```

## Методи властивостей
Окрім звичайних властивостей з данними, є можливість створити властивості у вигляді пари методів: 
- акцессора/геттера, який одержує поточне значення властивості
- мутатора/сеттера, який задає нове значення властивості

Наприклад:
```
var person = {
  firstName: 'John',
  lastName: 'Doe',

  get name() {
    return this.firstName + ' ' + this.lastName;
  },

  set name(value) {
    var split = value.split(' ');
    this.firstName = split[0];
    this.lastName = split[1];
  }
};

person.name; // John Doe

person.name = "Bill Jobs";

person.firstName; // Bill
person.lastName; // Jobs
```

> Можна оголосити тільки геттер, або тільки сеттер. Використовувати обидва методи не обов'язково. Таким чином
можна захистити властивість від запису/зчитування.

## Атрибути властивостей

- `[[Enumerable]]` - якщо true, то властивість "видно" в for..in і Object.keys(). Замовчування - false.
- `[[Configurable]]` - якщо true, то властивість можна видаляти, а також конфігурувати його за допомогою defineProperty. 
Замовчування false.
- `[[Writable]]` - якщо true, то значення можна змінювати. Замовчування false.
- `[[Value]]` - значення властивості. Замовчування undefined.

Для того, щоб створити властивість з відмінними від дефолтних атрибутами, потрібно використовувати метод defineProperty.

```
Object.defineProperty(obj, "superProperty", {
  value: "You can't change me! Ha-ha!",
  writable: false, // заборонити змінювати значення
  configurable: false, // заборонити видаляти та переконфігуровувати
  enumerable: false // не показувати при проходженні по об'єкту
});
```

Аналогічно можна створювати методи об'єктів:

```
var obj = {}

Object.defineProperty(obj, "superProperty", {

  get: function() {
    return "It's nothing here.";
  },

  set: function(value) {
      this._superProperty = "I have " + value + " inside me.";
    }
});
```

Створити декілька властивостей за один раз можна через defineProperties:
```
var obj = {}

Object.defineProperties(obj, {
  seven: {
    value: 7
  },

  eighth: {
    value: [8, 8.1]
  },

  ten: {
    get: function() {
      return "Microsoft style!";
    }
  }
});

```

# Prototype-based OOP
## Constructor

Constructor - функція, яка використовується для створення об'єктів. Описується як 
звичайна і функція. Відмінність тільки в імені функції-конструктора - його прийнято писати
з великої букви.

```
var Animal = function () { 
    // so empty
}

var dog = new Animal();
var car = new Animal();

dog instanceof Animal; // true
cat instanceof Animal; // true
```
Оператор `instanceof` дозволяє перевірити, чи є об'єкт екземпляром "классу". Аналогічну
перевірку можна зробити через перевірку властивості `constructor` об'єкту.
Конструктор при створенні об'єктів через літерал === `Object`.
 
Рекомендується використовувати для подібних перевірок саме `instanceof`, так як 
`constructor` об'єкту може бути перезаписаний.

Всередині конструктора можна створювати якийсь початковий стан об'єкту.
 
```
function Animal(name) {
this.name = name;
this.isAlive = true;
}

var dog = new Animal("dog");
```

Процес створення об'єкту:
1. Створюється порожній об'єкт
2. В `this` копіюється посилання на цей об'єкт
3. Створюються додані нами властивості
4. `this` _автоматично_ повертається як результат виконання функції

> Можна і явно прописати return всередині конструктора. Якщо спробувати повернути 
_не_ об'єкт, то return буде проігноровано - повернеться автоматично створений об'єкт.

> Ніколи не викликайте конструктор без `new`, інакше він може змінити глобальний об'єкт,
замість створення нового. (Викличе помилку в strict).

## Prototype

Властивість, яка є у всіх об'єктів і може мати посилання на інші об'єкти. Це дозволяє
шукати властивості в наступних об'єктах ланцюга прототипів, якщо його немає в нинішньому.

Наприклад, метод hasOwnProperty не копіюється кожного разу до кожного об'єкту, а 
міститься в об'єкті Object. Коли ми викликаємо `dog.hasOwnProperty()` відбувається наступний 
процес:

- Пошук цієї властивості у об'єкті `dog`
- (1): Якщо вона знайдена - зчитуємо її та повертаємо
- Якщо ні - зчитуємо prototype, отримуємо посилання на "батьківський" об'єкт
- Якщо посилання `null`, то припиняємо пошук і повертаємо undefined
- Шукаємо в отриманому об'єкті. 
- goto (1)

### [[Prototype]]

При створенні об'єкта через `new`, у внутрішню властивість нового об'єкта [[Prototype]] 
записується посилання на прототип конструктора.

![](http://speakingjs.com/es5/images/spjs_2103.png)

Значення прототипа можна отримати методом `Object.getPrototypeOf(obj)`.

Також можна перевірити, чи є один об'єкт прототипом іншого: 
`Object.prototype.isPrototypeOf(obj)`.

### Constructor ♥ Prototype

Хороша практика - виносити загальні функції в прототип.

Оголошені в конструкторі властивості копіюються в _кожен_ створений об'єкт, але якщо
винести їх в прототип конструктора, вони будуть існувати тільки в одному місці - 
об'єкті прототипу. Це дозволяє використовувати значно менше пам'яті.
 
Наприклад:
```
function Animal(name) {
    this.name = name;
    this.isAlive = true;
}

Animal.prototype.die = function () {
    this.isAlive = false;
}

var veryBadOldAnimal = new Animal("Human killer");
veryBadOldAnimal.die(); // veryBadOldAnimal.isAlive === false
```

> В прототипі можна оголошувати не тільки функції, а і загальні для всіх екземплярів 
константи.

Інший варіант запису:
```
function Animal(name) {
    this.name = name;
    this.isAlive = true;
}

Animal.prototype = {
    die: function() {
        this.isAlive = false;
    },
    changeName: function(newName) {
        this.name = newName;
    }
}
```

Його зручно використовувати, коли потрібно додати багато данних в прототип за один раз.
Але він повністю перезаписує прототип, в тому числі посилання на конструктор.
У цьому випадку він буде не Animal, а Object. 

Для того, щоб не вистрілити собі в %улюблена_частина_вашого_тіла%, потрібно явно його
вказати:
```
function Animal(name) {
    this.name = name;
    this.isAlive = true;
}

Animal.prototype = {
    constructor: Animal,
    
    die: function() {
        this.isAlive = false;
    },
    changeName: function(newName) {
        this.name = newName;
    }
}
```

> Методи в прототипі, як і все в JS, можна змінювати прямо на ходу ~~з вікна маршрутки~~
виконання коду.

## Прототипне наслідування
Для наслідування в JS використовується механізм прототипів. Цей підхід називається 
прототипним ооп/ланцюгом прототипів/прототипним наслідуванням і т.д.

При створенні екземпляру об'єкту, йому автоматично присвоюється прототип 
(який теж є об'єктом), і він має доступ до властивостей цього об'єкту прототипу. 
Таким чином, створений об'єкт наслідує ці властивості.

В свою чергу, у об'єкта прототипа є свій прототип, який дозволяє отримати доступ до 
його властивостей, і так далі. "Цар всіх звірів" - Object (Object.prototype), 
який є кінцевою точкою в більшості створених об'єктів.

> Прототипи вбудованих типів можна змінювати, але це не рекомендується, бо часто
приводить до конфліктів з іншими програмними рішеннями (н-д Prototype.js)

### Object.create
Рекомендований спосіб вказати батьківський ~~клас~~ об'єкт - метод Object.create.
Його перший параметр - об'єкт, який потрібно використовувати в якості прототипу,
другий і необов'язковий - це об'єкт опису властивостей (як defineProperties).

Перезапис прототипу призводить до того, що у дочірніх об'єктів конструктором 
буде вказаний батьківський конструктор. Правильний конструктор потрібно присвоїти явно.

```
function Animal(name) {
    this.name = name;
    this.alive = true;
}

Animal.prototype = {
    getName: function() {
        return this.name;
    }
};

function Doge(name) {
    this.name = name;
}

Doge.prototype = Object.create(Animal.prototype);
Doge.prototype.constructor = Doge; // вказуємо правильний конструктор

var swagDoge = new Doge('Swaggie');

swagDoge.getName(); // Swaggie
```

> Можна і напряму присвоїти об'єкт прототипу, але це не кращий підхід - потрібно 
створювати і ініціалізувати окремий екземпляр, який може хто зна як впливати на 
середовище.

Також можна створити "абсолютно порожній" об'єкт, який не буде наслідувати вбудовані
методи, через Object.create(null).

### Constructor stealing
Часто для правильної ініціалізації дочірнього об'єкта потрібно провести дії в 
конструкторі, які аналогічні батьківському. Щоб уникнути дублювання коду і збільшення
загальної кількості смертних гріхів в житті, використовують техніку 
'constructor stealing'. Вся суть - виклик батьківського конструктора в контекті 
дочірнього за допомогою call() або apply().

```
function Animal(name) {
    this.name = name;
    this.alive = true;
}

Animal.prototype = {
    getName: function() {
        return this.name;
    }
};

function Doge(name) {
    Animal.call(this, name);
    
    // додаткові данні
    this.furColor = 'golden';
}

Doge.prototype = Object.create(Animal.prototype);
Doge.prototype.constructor = Doge;

var swagDoge = new Doge('Swaggie');

swagDoge.getName(); // Swaggie
```

Аналогічно можна викликати методи батьківського _класу_:
```
[...]
Doge.prototype.constructor = Doge;

Doge.prototype = {
    getName: function() {
        var name = Animal.prototype.getName.call(this);
        
        return name + "! Wof-wof!";
    }
};

var swagDoge = new Doge('Swaggie');

swagDoge.getName(); // Swaggie! Wof-wof!
```