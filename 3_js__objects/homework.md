1.Написати "нативний" метод для повторення стрічки

```
"OGM!".repeat(3) // OGM!OGM!OGM!
```

2.Написати аналог Object.keys

```
myObjectKeys({
                firstName: 'Jason', 
                lastName: 'Script', 
                isAlive: 'maybe'
            });
            
// ['firstName', 'lastName', 'isAlive']            
```

3.Переписати з використанням прототипів
```
function DeathMachine(killPerSec, killMethod) {
  var humanKilled = 0,
	maxKillsPerSec = killPerSec || 1,
	method = killMethod || 'boil_in_hot_water', // nice default
	timeout = 0,
	needToKill = 0;

  function getTimeToApocalypse() {
    return timeout;
  }

  this.run = function() {
    setTimeout(function() {
      if (humanKilled < needToKill) {
        console.log( 'my destiny is Hell' );
      }  else {
        console.log('I soooo am saaaad. I don\'t want to do it...')
      }
    }, getTimeToApocalypse());
  };

  this.setup = function(delay) {
    timeout = delay;
    needToKill = delay * maxKillsPerSec;
  };
}

var boilDeathMachine = new DeathMachine(1);
boilDeathMachine.setup(2);
boilDeathMachine.run();
```

4.Створити систему для збереження паролів:

- клас PasswordManager
    - містить в собі список всіх акаунтів
    - має метод для зміни мастер-паролю
    - мастер пароль можна задати на етапі ініціалізації
    - має метод для додавання акаунту
    - має методи для отримання паролю по адресі сайту. 
    Для доступу потрібен мастер-пароль (загальний для всіх 
    методів)
- клас Account
    - складається з імені акаунта, адреси сайту та пароля
    - пароль є властивістю доступною для запису/читання з 
    простою валідацією (довжина більше 8 символів)
    - має метод для генерації паролю (байдуже як, хоч +new Date)
    
4.1Розширити можливості менеджера паролів:

- Акаунти можуть бути різних типів 
    - для сайтів (все ще містить адресу)
    - для карт (немає адреси сайту, містить номер карти та код 
    доступу (ті самі три цифри на тильній стороні карти) 
    ~~вписуйте реальні данні і присилайте на пошту~~)
    - для програм (немає адреси сайту)
- пароль (де він є) можна задати при ініціалізації (або не задати =))
- У об'єкта акаунта повинна бути власна назва   
- Відповідно, в самому менеджері отримати пароль можна по 
цій унікальній назві та мастер-паролю, а не по адресі сайту 
та мастер-паролю

Типи акаунтів є підтипами (нащадками) класу Account. Їх назви 
довільні.