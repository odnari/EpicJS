### Практика
Дописати код, щоб всі тести проходи успішно. 

[CodePen](https://codepen.io/nnbpy/pen/dpvJdB?editors=0011)

> Для роботи робіть форк пену 

##### 1. Що виведе console.log?
1.1
```
justDoIt('learn');

var subj = 'Javascript';

function justDoIt(action) {
    console.log(action + ' ' + subj);
}
```
1.2
```
justDoIt('learn');

var subj = 'Javascript';

var justDoIt = function (action) {
    console.log(action + ' ' + subj);
}
```
##### 2. Яким буде значення змінної foo після виконання
2.1
```
var foo = 0;

function bar() {
    foo = 2;

    return foo ;
}

bar();

```

2.2
```
var foo = 0;

function bar() {
    foo = 2;
    
    if ({}) {
        var foo = 3;
    } else {
        foo = 4
    }

    return foo;
}

bar();

```
#### 3. Який буде результат виконання коду
3.1
```
function bar() {
   
 alert( window );

 var window = 5;

 alert( window );
}

bar();

```