# Reading list
## HTML
- [Хороший набір матеріалів для повторення](https://developer.mozilla.org/ru/docs/Web/Guide/HTML)
- [Набір кращих практик](http://wtfhtmlcss.com)
- [Міні-курс по побудові структури сторінок](http://ru.learnlayout.com)
- [Велика кількість прикладів всеможливих макетів](http://htmlbook.ru/samlayout/tipovye-makety)
- [Кодстайл](http://codeguide.co/)

## CSS
- [Для оновлення базових знань](https://developer.mozilla.org/ru/docs/Web/Guide/CSS)
- [Практика вибору елементів у вигляді "гри"](https://flukeout.github.io)
- [Більше про медіа-вирази](https://webref.ru/css/media)
- [Коллапс відступів (корисно і потрібно знати)](http://htmlbook.ru/samlayout/blochnaya-verstka/skhlopyvayushchiesya-otstupy)
- [Про псевдоселектори типу](https://css-tricks.com/the-difference-between-nth-child-and-nth-of-type)
- [Грід-системи на базі fload](https://www.sitepoint.com/understanding-css-grid-systems)
- [Грід-системи на базі flexbox](https://css-tricks.com/dont-overthink-flexbox-grids)

